from flask import request, jsonify, url_for
from app.models import Card
from app.api import bp
from app import db
from app.api.errors import bad_request



@bp.route('/cards/<int:id>', methods=['GET'])
def get_card(id):
    # Возвращаем пользователю информацию о сервисной карте
    # Если сервисной карты с таким уникальным идентификатором нет,
    # возвращаем HTTP 404
    return jsonify(Card.query.get_or_404(id).to_dict())


@bp.route('/cards', methods=['GET'])
def get_cards():
    # Какую страницу показать? По умолчанию 1
    page = request.args.get('page', 1, type=int)
    # Сколько элементов на странице? По умолчанию 10
    # Но не больше 100
    per_page = min(request.args.get('per_page', 10), 100)
    # Генерируем набор данных для страницы
    data = Card.to_collection_dict(Card.query, page, per_page, 'api.get_cards')
    # Возвращаем пользователю json
    return jsonify(data)

@bp.route('/cards', methods=['POST'])
def create_card():
    print(request.data)
    # Проверяем, что запрос пришел с телом
    data = request.get_json() or {}
    # Если тела запроса нет, возвращаем ошибку
    if not data:
        return bad_request('Card should contain something')
    # Создаем экземпляр модели ORM для сервисной карты
    card = Card()
    # Загружаем данные из тела запроса в экземпляр
    card.from_dict(data)
    # Добавляем машину к текущей сессии с БД 
    db.session.add(card)
    # Комиттим транзакцию в БД
    db.session.commit()
    # По стандарту мы должны вернуть объект
    # с присвоенным уникальным идентификатором
    response = jsonify(card.to_dict())
    # Так же по стандарту код ответа должен быть 201 вместо 200
    # 200 OK
    # 201 Created
    response.status_code = 201
    # В заголовке передаем ссылку на созданный объект
    response.headers['Location'] = url_for('api.get_card', id=card.id)
    return response


@bp.route('/cards/<int:id>', methods=['PUT'])
def update_card(id):
    card = Card.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Build should contain something')
    card.from_dict(data)
    db.session.commit()
    return jsonify(card.to_dict())


@bp.route('/cards/<int:id>', methods=['DELETE'])
def remove_car(id):
    card = Card.query.get_or_404(id)
    db.session.delete(card)
    db.session.commit()
    return jsonify(card.to_dict())
